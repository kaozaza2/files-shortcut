package com.mikore.shortcut.files;

import android.app.Activity;
import android.os.Bundle;
import android.content.ComponentName;
import android.content.Intent;
import android.widget.Toast;
import android.util.Log;

public class MainActivity extends Activity {
    
    private static final String[] packageNameList = {
        "com.google.android.documentsui",
        "com.android.documentsui"
    };
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for (String packageName : packageNameList) {
            if (isPackageExists(packageName)) {
                Intent i = getPackageManager()
                    .getLaunchIntentForPackage(packageName);
                if (i != null) {
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                    return;
                }
            }
        }
        Toast.makeText(
            this,
            "Files not available on you device!",
            Toast.LENGTH_LONG
        ).show();
        finish();
    }
    
    private boolean isPackageExists(String packageName) {
        try {
            getPackageManager().getPackageInfo(packageName, 0);
            return true;
        } catch (Exception e) {
            Toast.makeText(
                this,
                Log.getStackTraceString(e),
                Toast.LENGTH_LONG
            ).show();
            return false;
        }
    }
}
